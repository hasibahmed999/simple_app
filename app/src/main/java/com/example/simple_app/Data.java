package com.example.simple_app;

import java.util.ArrayList;
import java.util.List;

public class Data  {

    String name;
    List<String> items = new ArrayList<String>();

    public void setItem(String item){
        items.add(item);
    }

    public List<String> getitems(){
        return items;
    }
    public String getName(){
        return this.name;
    }

    Data(String name){
        this.name=name;
    }
}
