package com.example.simple_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Data data;
    private ExpandableListView expandableListView;
    private ExpandableListViewAdapter expandableListViewAdapter;
    private List<String> listDataGroup;
    private HashMap<String, List<String>> listDataChild;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // initializing the views
        initViews();
        // initializing the listeners
        initListeners();
        // initializing the objects
        initObjects();
        // preparing list data
        initListData();
    }
    /**
     * method to initialize the views
     */
    private void initViews() {
        expandableListView = findViewById(R.id.expandableListView);
    }
    /**
     * method to initialize the listeners
     */
    private void initListeners() {
        // ExpandableListView on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        listDataGroup.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataGroup.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
        // ExpandableListView Group expanded listener
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        listDataGroup.get(groupPosition),
                        Toast.LENGTH_SHORT).show();
            }
        });
        // ExpandableListView Group collapsed listener
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        listDataGroup.get(groupPosition),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    /**
     * method to initialize the objects
     */
    private void initObjects() {
        // initializing the list of groups
        listDataGroup = new ArrayList<>();
        // initializing the list of child
        listDataChild = new HashMap<>();
        // initializing the adapter object
        expandableListViewAdapter = new ExpandableListViewAdapter(this, listDataGroup, listDataChild);
        // setting list adapter
        expandableListView.setAdapter(expandableListViewAdapter);
    }
    /*
     * Preparing the list data
     *
     * Dummy Items
     */
    private void initListData() {

        

        // Adding group data
        listDataGroup.add(" "+getString(R.string.txt_chapter_1));
        listDataGroup.add(" "+getString(R.string.txt_chapter_2));
        listDataGroup.add(" "+getString(R.string.txt_video));
        listDataGroup.add(" "+getString(R.string.txt_document));
        // array of strings
        String[] array;
        // list of Chapter_1
        List<String> chapter_1_list = new ArrayList<>();
        array = getResources().getStringArray(R.array.list_chapter_1);
        for (String item : array) {
            chapter_1_list.add(item);
        }

        List<String> chapter_2_list = new ArrayList<>();
        array = getResources().getStringArray(R.array.list_chapter_2);
        for (String item : array) {
            chapter_2_list.add(item);
        }

        List<String> video_list = new ArrayList<>();


        List<String> document_list = new ArrayList<>();

        // Adding child data
        listDataChild.put(listDataGroup.get(0), chapter_1_list);
        listDataChild.put(listDataGroup.get(1), chapter_2_list);
        listDataChild.put(listDataGroup.get(2), video_list);
        listDataChild.put(listDataGroup.get(3), document_list);
        // notify the adapter
        expandableListViewAdapter.notifyDataSetChanged();
    }
}